const express = require('express');
const bcrypt = require('bcrypt');
const {Client} = require('pg');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const cors = require('cors');
const app = express();
const port = 3001;
const saltRounds = 10;
const JSON_SECRET = '3bd561c37d214b4496d09049fadchellobye';

app.use(bodyParser.json())
app.use(cors())

const client = new Client({
	user: 'postgres',
	database : 'postgres',
	host: 'postgres'
})

client.connect()
        .then(()=>{
		console.log('Im connected');
	})
	.catch((e)=>{
		console.log(e);
	});


app.post('/register', (req, res ) => {
	console.log(req.body);
	const {
		username,
		password
	} = req.body;

	bcrypt.genSalt(saltRounds, function(err, salt) {
		bcrypt.hash(password, salt, function(err, hash) {
			return client.query('insert into public.user(username, password) values($1,$2) returning *',[
				username,
				hash
			])
			.then(()=>{
				res.send({
					'message' : 'done successful'
				});
			})
			.catch((e)=>{
				res.send({
					'message' : e.message
				}).status(500);
			});
		});
	});
});

app.post('/login', (req, res ) => {
	const {
		username,
		password
	} = req.body;

	return client.query(`select * from public.user where username=$1`,
	[
		username
	])
		.then((data)=>{
			if(data.rows.length) {
				const user = data.rows.shift();
				return bcrypt.compare(password, user.password).then(function(confirmed) {
					if(!confirmed) {
						return res.send({
							'message' : 'Wrong password'
						}).status(403)
					}

					const token = jwt.sign({
						name: user.username,
						"https://hasura.io/jwt/claims": {
							"x-hasura-allowed-roles": ["admin"],
							"x-hasura-default-role": "admin",
							"x-hasura-user-id": `"${user.id}"`
						}
					}, JSON_SECRET);
					
					return res.send({
						message : 'Goods',
						token
					});
				});
			}
			return res.send({
				'message' : 'Not Found'
			}).status(404);
		})
		.catch((e)=>{
			res.send({
				'message' : e.message
			}).status(500);
		});
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
